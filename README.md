# Prerequisites
First clone the git submodules:
```sh
git submodule init
git submodule update
```
to then ignore the submodules when running `git status` in the root directory,
run:
```sh
git config submodule.busybox.ignore all
git config submodule.kernel.ignore all
```

## Busybox
```sh
cd busybox
make menuconfig
```
Load the config file at `../config/busybox_config` and then run:
```sh
make
make install
```

## Kernel
```sh
cd kernel
# Start with a preset.
make ARCH=x86_64 x86_64_defconfig
make menuconfig
```
Load the config file at `../config/kernel_config` and then run:
```sh
make
```

# How to use the scripts
## Build initramfs
Run the following command to build `initramfs`:
```sh
bash build_initramfs.sh
```
which will create copy the `root/initramfs` folder into `build`, will add the
rest of the files including busybox and will create the compressed initramfs
file.

*Note: We are not being strict about what should be in initramfs and what should
be mounted.*

## Create disk
Very simple script to create a disk image that can be mounted in the host and
in the guest filesystem.
```sh
bash create_hda.sh
```

## Run the custom kernel in the custom initramfs image
```sh
bash run.sh
```
To exit, press CTRL-C.

# Eudyptula solutions
You can find the schematic high level solution to the exercises in each
`exercises/exercise_n.md` file. If there is a module or script needed, you can
find it in `exercises/exercise_n` folder or it has been incorporated into the
init scripts.
