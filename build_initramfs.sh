#!/bin/bash
mkdir -p build
sudo rm -rf build/initramfs
cp -r initramfs build/initramfs

cd build/initramfs
# Folder structure
mkdir -p bin dev etc home mnt proc sys usr
# Busybox
cp -av ../../busybox/_install/* .
# dev/*
sudo mknod dev/sda b 8 0
sudo mknod dev/console c 5 1

# Create the image
find . -print0 | cpio --null -ov --format=newc | gzip -9 > ../initrd_x86_64.gz
