// SPDX-License-Identifier: GPL-2.0
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Angel Alberto Carretero");
MODULE_DESCRIPTION("A simple kernel module used for debug printing");

static int __init module_example_init(void)
{
	printk(KERN_INFO "Hello world!\n");
	return 0;
}

static void __exit module_example_exit(void)
{
	printk(KERN_INFO "Goodbye world!\n");
}

module_init(module_example_init);
module_exit(module_example_exit);
