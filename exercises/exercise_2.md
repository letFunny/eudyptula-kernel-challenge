# In kernel folder, to copy the existing default config.
make ARCH=x86_64 x86_64_defconfig

# Manually update it to contain debug symbols.
make menuconfig
