# Hotplug prerequisites
This is another way of testing the module by changing the configs and not doing
the `modprobe`. When connecting the keyboard the module should be loaded
automatically.

## Kernel config
Set this flag in the kernel:
```sh
CONFIG_UEVENT_HELPER=y
CONFIG_UEVENT_HELPER_PATH=/sbin/hotplug
```

## Init changes

## /etc/passwd and /etc/group
Under `/etc/passwd`:
```sh
root:x:0:0:root:/root:/bin/sh
```
and under `/etc/group`:
```sh
root:x:0:root
```

## mdev config
To `modprobe` all the drivers that match, under `/etc/mdev.conf`:
```sh
$MODALIAS=.*	root:root	0660	@modprobe -b "$MODALIAS"
```

## Busybox config
You have to remove the option for:
```sh
CONFIG_MODPROBE_SMALL
```

# 1. Load module
```sh
cp /mnt/kb_notification_module.ko /lib/modules/$(uname -r)
depmod -a
modprobe kb_notification_module
```

# 2. How to plug keyboard
In QEMU console, CTRL+ALT+2 use:
```sh
device_add usb-kbd
```
Note: you should have "-usb" flag in the QEMU run command.
