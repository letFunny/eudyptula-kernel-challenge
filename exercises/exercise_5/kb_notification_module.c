// SPDX-License-Identifier: GPL-2.0
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>
#include <linux/hid.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Angel Alberto Carretero");

/* table of devices that work with this driver */
static struct usb_device_id usb_kb_notification_table [] = {
        { USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,
            USB_INTERFACE_SUBCLASS_BOOT,
            USB_INTERFACE_PROTOCOL_KEYBOARD) },
        { }
};

static int usb_kb_notification_probe(struct usb_interface *interface,
		const struct usb_device_id *id)
{
		printk(KERN_INFO "Keyboard connected!\n");
		return 0;
}

static void usb_kb_notification_disconnect(struct usb_interface *interface)
{
		printk(KERN_INFO "Keyboard disconnected!\n");
}

static struct usb_driver usb_kb_notification_driver = {
        .name        = "usb_kb_notification",
        .probe       = usb_kb_notification_probe,
        .disconnect  = usb_kb_notification_disconnect,
        .id_table    = usb_kb_notification_table,
};

static int __init usb_kb_notification_init(void)
{
        int result;

		printk(KERN_INFO "Registering usb_kb_notification module!\n");
        /* register this driver with the USB subsystem */
        result = usb_register(&usb_kb_notification_driver);
        if (result < 0) {
                pr_err("usb_register failed for the %s driver. Error number %d\n",
                       usb_kb_notification_driver.name, result);
                return -1;
        }

        return 0;
}

static void __exit usb_kb_notification_exit(void)
{
		printk(KERN_INFO "Deregistering usb_kb_notification module!\n");
        /* deregister this driver with the USB subsystem */
		usb_deregister(&usb_kb_notification_driver);
}

MODULE_DEVICE_TABLE (usb, usb_kb_notification_table);

module_init(usb_kb_notification_init);
module_exit(usb_kb_notification_exit);
