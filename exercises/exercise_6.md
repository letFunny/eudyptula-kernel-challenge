# 1. Load module
```sh
cp /mnt/id_char_device.ko /lib/modules/$(uname -r)
depmod -a
modprobe id_char_device
```

# 2. Read operation
```sh
cat /dev/eudyptula
```
The output is `my_id` (without newline).

# 3. Write operation
```sh
echo "whatever" > /dev/eudyptula
```
The output is `sh: write error: Invalid argument`. However, when we try to write
the proper id instead:
```sh
echo -n "my_id" > /dev/eudyptula
```
the return code is success.
