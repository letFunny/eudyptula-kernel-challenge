// SPDX-License-Identifier: GPL-2.0
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/string.h>

#define MY_ID "my_id"
#define MY_ID_LEN 5

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Angel Alberto Carretero");

static ssize_t id_char_write(struct file *file, const char __user *buf,
               size_t len, loff_t *ppos)
{
	char id[] = MY_ID;

	printk(KERN_INFO "id_char misc device write operation\n");
	if (0 == strncmp(buf, id, MY_ID_LEN + 1)) {
		return MY_ID_LEN;
	} else {
		return -1;
	}
}

static ssize_t id_char_read(struct file *filp, char __user *buf,
                    size_t count, loff_t *f_pos)
{
	char id[] = MY_ID;
	ssize_t ret;

	printk(KERN_INFO "id_char misc device read operation\n");
	if (*f_pos >= MY_ID_LEN + 1) {
		return 0;
	}

	ret = strlcpy(buf + *f_pos, id, count);
	ret = ret < count ? ret : count;
	*f_pos += ret;
	return ret;
}

// File operation structure.
static const struct file_operations fops = {
    .owner          = THIS_MODULE,
    .write          = id_char_write,
    .read           = id_char_read,
    .llseek         = no_llseek,
};

// Misc device structure.
struct miscdevice id_char_misc_device = {
		.minor = MISC_DYNAMIC_MINOR,
		.name = "eudyptula",
		.fops = &fops,
};

static int __init id_char_init(void)
{
		int error;

		printk(KERN_INFO "Registering id_char as misc device\n");
		error = misc_register(&id_char_misc_device);
		if (error) {
				printk(KERN_INFO "Failed registering id_char as misc device\n");
				return error;
		}

        return 0;
}

static void __exit id_char_exit(void)
{
		printk(KERN_INFO "Unloading id_char module!\n");
		misc_deregister(&id_char_misc_device);
}

module_init(id_char_init);
module_exit(id_char_exit);

