# 1. Load module
```sh
cp /mnt/debugfs.ko /lib/modules/$(uname -r)
depmod -a
modprobe debugfs
```

# 2. File: id
See exercise_6 for how to test it but use `/sys/kernel/debug/eudyptula/id`
instead.

# 3. File: jiffies
Running `cat /sys/kernel/debug/eudyptula/jiffies` outputs the number of
interrupts since initializing the kernel.

# 4. File: foo
When writing to the file it will record the contents for the next reads,
example:
```sh
$ cd /sys/kernel/debug/eudyptula
$ cat foo
$ echo foo > foo && cat foo
foo
$ echo "foo bar baz" > foo && cat foo
foo bar baz
$ echo "short" > foo && cat foo
short
```

# 5. Permissions
Running `ls -l` produces the following output:
```sh
total 0
-rw-r--r--    1 root     root             0 Mar 26 14:41 foo
-rw-rw-rw-    1 root     root             0 Mar 26 14:41 id
-r--r--r--    1 root     root             0 Mar 26 14:41 jiffies
```
