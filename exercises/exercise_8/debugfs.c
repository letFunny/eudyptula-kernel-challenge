// SPDX-License-Identifier: GPL-2.0
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/debugfs.h>

#define MY_ID "my_id"
#define MY_ID_LEN 5
#define JIFFIES_STR_SIZE 20

static uint8_t foo_storage[PAGE_SIZE];
static size_t foo_storage_len = 0;
static DEFINE_MUTEX(foo_storage_mutex);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Angel Alberto Carretero");

static ssize_t id_char_write(struct file *file, const char __user *buf,
	size_t len, loff_t *ppos)
{
	char id[] = MY_ID;

	printk(KERN_INFO "id_char write operation\n");
	if (0 == strncmp(buf, id, MY_ID_LEN + 1)) {
		return MY_ID_LEN;
	} else {
		return -1;
	}
}

static ssize_t id_char_read(struct file *filp, char __user *buf,
	size_t count, loff_t *f_pos)
{
	char id[] = MY_ID;
	ssize_t ret = 0;

	printk(KERN_INFO "id_char read operation\n");
	if (*f_pos >= MY_ID_LEN + 1) {
		return 0;
	}

	ret = strlcpy(buf + *f_pos, id, count);
	ret = ret < count ? ret : count;
	*f_pos += ret;
	return ret;
}

static ssize_t jiffies_read(struct file *filp, char __user *buf, size_t count,
	loff_t *f_pos)
{
	// Max side of u64 + 1 for NULL byte.
	static char jiffies_str[JIFFIES_STR_SIZE];
	static size_t jiffies_str_len;
	ssize_t ret = 0;

	printk(KERN_INFO "jiffies read operation\n");
	// If we are starting the read copy the number.
	if (*f_pos == 0) {
		if (0 == snprintf(jiffies_str, JIFFIES_STR_SIZE, "%llu", jiffies_64)) {
			printk(KERN_ERR "jiffies counter could not be formatted into a"
					"string\n");
			return -1;
		}
		jiffies_str_len = strnlen(jiffies_str, JIFFIES_STR_SIZE);
	}

	// For the next reads return the string in `count` size steps.
	if (*f_pos >= jiffies_str_len) {
		return 0;
	}

	// Copy the string length or less, depending on count.
	count = jiffies_str_len < count ? jiffies_str_len : count;
	ret = count - copy_to_user(buf + *f_pos, jiffies_str, count - *f_pos);
	*f_pos = ret;
	return ret;
}

static ssize_t foo_read(struct file *filp, char __user *buf, size_t count,
	loff_t *f_pos)
{
	ssize_t ret = 0;

	printk(KERN_INFO "foo read operation\n");

	if (*f_pos >= foo_storage_len) {
		return 0;
	}

	count = foo_storage_len < count ? foo_storage_len : count;
	mutex_lock(&foo_storage_mutex);
	ret = count - copy_to_user(buf + *f_pos, foo_storage, count);
	*f_pos += ret;
	mutex_unlock(&foo_storage_mutex);

	return ret;
}

static ssize_t foo_write(struct file *file, const char __user *buf,
	size_t len, loff_t *ppos)
{
	ssize_t ret;

	printk(KERN_INFO "foo write operation\n");
	if (*ppos >= PAGE_SIZE) {
		return 0;
	} else if (*ppos + len >= PAGE_SIZE) {
		// Write only until PAGE_SIZE.
		len = PAGE_SIZE - *ppos;
	}

	mutex_lock(&foo_storage_mutex);
	ret = len - copy_from_user(foo_storage + *ppos, buf, len);
	*ppos += ret;
	foo_storage_len = ret;
	mutex_unlock(&foo_storage_mutex);

	return ret;
}

// File operation structure.
static const struct file_operations id_char_fops = {
    .owner          = THIS_MODULE,
    .write          = id_char_write,
    .read           = id_char_read,
    .llseek         = no_llseek,
};
static const struct file_operations jiffies_fops = {
    .owner          = THIS_MODULE,
    .read           = jiffies_read,
    .llseek         = no_llseek,
};
static const struct file_operations foo_fops = {
    .owner          = THIS_MODULE,
    .read           = foo_read,
	.write          = foo_write,
    .llseek         = no_llseek,
};

static int __init debugfs_init(void)
{
	struct dentry *dir;
	struct dentry *id;
	struct dentry *jiffies;
	struct dentry *foo;
	umode_t id_umode;
	umode_t jiffies_umode;
	umode_t foo_umode;

	printk(KERN_INFO "Loading debugfs\n");
	dir = debugfs_create_dir("eudyptula", NULL);
	if (IS_ERR(dir)) {
		printk(KERN_ERR "Failed creating the debugfs directory\n");
		return PTR_ERR(dir);
	}

	id_umode = (umode_t) (S_IROTH | S_IWOTH | S_IWGRP | S_IRGRP | S_IWUSR |
			S_IRUSR);
	id = debugfs_create_file("id", id_umode, dir, NULL, &id_char_fops);
	if (IS_ERR(id)) {
		printk(KERN_ERR "Failed creating the id file\n");
		return PTR_ERR(id);
	}

	jiffies_umode = (umode_t) (S_IROTH | S_IRGRP | S_IRUSR);
	jiffies = debugfs_create_file("jiffies", jiffies_umode, dir, NULL,
			&jiffies_fops);
	if (IS_ERR(jiffies)) {
		printk(KERN_ERR "Failed creating the jiffies file\n");
		return PTR_ERR(jiffies);
	}

	foo_umode = (umode_t) (S_IROTH | S_IRGRP | S_IWUSR | S_IRUSR);
	foo = debugfs_create_file("foo", foo_umode, dir, NULL,
			&foo_fops);
	if (IS_ERR(foo)) {
		printk(KERN_ERR "Failed creating the foo file\n");
		return PTR_ERR(foo);
	}

	return 0;
}

static void __exit debugfs_exit(void)
{
	printk(KERN_INFO "Unloading debugfs module!\n");
}

module_init(debugfs_init);
module_exit(debugfs_exit);

