# 1. Load module
```sh
cp /mnt/sys.ko /lib/modules/$(uname -r)
depmod -a
modprobe sys
```

# 2. File: id
See exercise_6 for how to test it but use `/sys/kernel/eudyptula/id`
instead.

# 3. File: jiffies
See exercise_8 but with `/sys/kernel/eudyptula/jiffies` instead.

# 4. File: foo
See exercise_8 but with `/sys/kernel/eudyptula/foo` instead.

# 5. Permissions
Running `ls -l` produces the following output:
```sh
total 0
-rw-r--r--    1 root     root          4096 Mar 26 20:15 foo
-rw-rw-r--    1 root     root          4096 Mar 26 20:15 id
-r--r--r--    1 root     root          4096 Mar 26 20:15 jiffies
```
