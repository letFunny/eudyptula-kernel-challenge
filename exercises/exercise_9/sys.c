// SPDX-License-Identifier: GPL-2.0
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/sysfs.h>
#include <linux/errno.h>
#include <linux/string.h>

#define MY_ID "my_id"
#define MY_ID_LEN 5
#define JIFFIES_STR_SIZE 20

static uint8_t foo_storage[PAGE_SIZE];
static size_t foo_storage_len = 0;
static DEFINE_MUTEX(foo_storage_mutex);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Angel Alberto Carretero");

static ssize_t id_char_write(struct kobject *kobj, struct kobj_attribute *attr,
	const char *buf, size_t count)
{
	char id[] = MY_ID;

	printk(KERN_INFO "id_char write operation\n");
	if (0 == strncmp(buf, id, MY_ID_LEN + 1)) {
		return MY_ID_LEN;
	} else {
		return -1;
	}
}

static ssize_t id_char_read(struct kobject *kobj, struct kobj_attribute *attr,
	char *buf)
{
	char id[] = MY_ID;

	printk(KERN_INFO "id_char read operation\n");

	// Copy the string 0 delimiter.
	memcpy(buf, id, MY_ID_LEN + 1);
	return MY_ID_LEN + 1;
}

static ssize_t jiffies_read(struct kobject *kobj, struct kobj_attribute *attr,
	char *buf)
{
	printk(KERN_INFO "jiffies read operation\n");
	// TODO change return to -1 when count != 0 and ret == 0. Also in previous
	// exercises.
	return sysfs_emit(buf, "%llu", jiffies_64);
}

static ssize_t foo_read(struct kobject *kobj, struct kobj_attribute *attr,
	char *buf)
{
	printk(KERN_INFO "foo read operation\n");

	mutex_lock(&foo_storage_mutex);
	memcpy(buf, foo_storage, foo_storage_len);
	mutex_unlock(&foo_storage_mutex);

	return foo_storage_len;
}

static ssize_t foo_write(struct kobject *kobj, struct kobj_attribute *attr,
	const char *buf, size_t count)
{
	printk(KERN_INFO "foo write operation\n");
	// Write only until PAGE_SIZE.
	count = count >= PAGE_SIZE ? PAGE_SIZE : count;

	mutex_lock(&foo_storage_mutex);
	memcpy(foo_storage, buf, count);
	foo_storage_len = count;
	mutex_unlock(&foo_storage_mutex);

	return count;
}

// Source: https://elixir.bootlin.com/linux/latest/source/samples/kobject/kobject-example.c
// TODO file premissions
static struct kobj_attribute foo_attribute =
	__ATTR(foo, (S_IROTH | S_IRGRP | S_IWUSR | S_IRUSR), foo_read, foo_write);
static struct kobj_attribute jiffies_attribute =
	__ATTR(jiffies, (S_IROTH | S_IRGRP | S_IRUSR), jiffies_read, NULL);
// Remove S_IWOTH because it is not allowed in sysfs files.
static struct kobj_attribute id_char_attribute =
	__ATTR(id, (S_IROTH | S_IWGRP | S_IRGRP | S_IWUSR | S_IRUSR), id_char_read,
			id_char_write);

static struct attribute *attrs[] = {
	&foo_attribute.attr,
	&jiffies_attribute.attr,
	&id_char_attribute.attr,
	NULL,	/* need to NULL terminate the list of attributes */
};

/*
 * An unnamed attribute group will put all of the attributes directly in
 * the kobject directory. If we specify a name, a subdirectory will be
 * created for the attributes with the directory being the name of the
 * attribute group.
 */
static struct attribute_group attr_group = {
	.attrs = attrs,
};

static struct kobject *dir_kobj;

static int __init sys_init(void)
{
	int retval = 0;

	dir_kobj = kobject_create_and_add("eudyptula", kernel_kobj);
	if (NULL == dir_kobj)
		return -ENOMEM;

	// Create the files associated with this kobject.
	retval = sysfs_create_group(dir_kobj, &attr_group);
	if (retval)
		kobject_put(dir_kobj);

	return retval;
}

static void __exit sys_exit(void)
{
	printk(KERN_INFO "Unloading sys module!\n");
	kobject_put(dir_kobj);
}

module_init(sys_init);
module_exit(sys_exit);

