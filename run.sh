qemu-system-x86_64 -s -kernel kernel/arch/x86/boot/bzImage \
    -initrd build/initrd_x86_64.gz \
    -hda build/hda.img \
    -boot c -m 2049M -append "root=/dev/sda rw console=ttyS0,115200 acpi=off nokaslr" \
    -serial stdio \
    -usb
    # -boot c -m 2049M -append "root=/dev/sda rw acpi=off nokaslr" \
    # -display none
    # USB 2.0
    # -device usb-ehci,id=usb,bus=pci.0,addr=0x4 \

